<?php

namespace Meteo;
use Laminas\Router\Http\Segment;
return [

    'router' => [
        'routes' => [
            'meteo' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/meteo[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\MeteoController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'post' => __DIR__ . '/../view',
        ],
    ],
];
