<?php

namespace Wines\Controller;

// Add the following import:
use Wines\Form\WineForm;
use Wines\Form\CartForm;
use Wines\Model\WinesTable;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;


class WinesController extends AbstractActionController {

    // Add this property:
    private $table;

    // Add this constructor:
    public function __construct(WinesTable $table) {
        $this->table = $table;
        session_start();
    }

public function indexAction()
{
    // Grab the paginator from the AlbumTable:
    $paginator = $this->table->fetchAll(true);

    // Set the current page to what has been passed in query string,
    // or to 1 if none is set, or the page is invalid:
    $page = (int) $this->params()->fromQuery('page', 1);
    $page = ($page < 1) ? 1 : $page;
    $paginator->setCurrentPageNumber($page);

    // Set the number of items per page to 10:
    $paginator->setItemCountPerPage(10);

    return new ViewModel(['paginator' => $paginator]);
}
    public function addAction() {
        
    }

    public function showAction() {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('wines', ['action' => 'add']);
        }

        // Retrieve the album with the specified id. Doing so raises
        // an exception if the album is not found, which should result
        // in redirecting to the landing page.
        try {
            $wine = $this->table->getWine($id);

            // return new ViewModel([
            //     'wine' => $this->table->getWines($id)
            // ]);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('wines', ['action' => 'index']);
        }


        $form = new WineForm();
        $form->bind($wine);
        $form->get('quantity')->setAttribute('value', '1');
        $form->get('submit')->setAttribute('value', 'Add To Cart');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form, 'wine' => $this->table->getWine($id)];

        if (!$request->isPost()) {
            return $viewData;
        }
    }

    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('wines');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                
                $i = 0;
                foreach ($_SESSION['cart']['wine'] as $wineToDel) {
                    if ($wineToDel->id == $id) {
                        unset($_SESSION['cart']['wine'][$i]);
                    }

                    $i++;
                }
                $_SESSION['cart']['wine'] = array_values($_SESSION['cart']['wine']);
                //$this->table->deleteWines($id);
            }

            if (count($_SESSION['cart']['wine']) == 0) {
                unset($_SESSION['cart']['wine']);
            }
            // Redirect to cart
            return $this->redirect()->toRoute('wines', ['action' => 'cart']);
        }

        return [
            'id' => $id,
            'wine' => $this->table->getWine($id),
        ];
    }

    public function cartAction() {

        $id = (int) $this->params()->fromRoute('id', 0);


        if (0 === $id) {
            return new ViewModel([
            ]);
            //return $this->redirect()->toRoute('wines', ['action' => 'index']);
        }


        $wine = $this->table->getWine($id);

        if (isset($_SESSION['cart']['wine'])) {

            foreach ($_SESSION['cart']['wine'] as $wineSession) {
                if ($wineSession->code == $wine->code) {

                    $existIncart = true;

                    break;
                } else {

                    $existIncart = false;
                }
            }
        }

        if (isset($existIncart) && $existIncart == true) {
            $i = 0;
            foreach ($_SESSION['cart']['wine'] as $aWine) {

                if ($aWine->code == $wine->code) {

                    $aWine->quantity += $_POST['quantity'];
                    $_SESSION['cart']['wine'][$i] = $aWine;
                }
                $i++;
            }
        } else {
            if (isset($_SESSION['cart']['wine'])) {
                $wine->quantity = $_POST['quantity'];
                array_push($_SESSION['cart']['wine'], $wine);
            } else {
                $wine->quantity = $_POST['quantity'];
                $_SESSION['cart']['wine'][] = $wine;
            }
        }





        $i = 0;
        foreach ($_SESSION['cart']['wine'] as $cartForm) {

            $form[$i] = new CartForm($_SESSION['cart']['wine']);
            $form[$i]->bind($cartForm);
            $form[$i]->get('quantity')->setAttribute('value', $cartForm->quantity);
            $i++;
        }
        $i--;
        $form[$i]->get('submit')->setAttribute('value', 'Checkout');



        $viewData = ['id' => $id, 'form' => $form];

        //  if (!$form->isValid()) {
        return $viewData;
        //}
    }

    public function deleteCartAction() {



        unset($_SESSION["cart"]);
        return $this->redirect()->toRoute('wines', ['action' => 'cart']);
    }

    public function checkoutAction() {


        $i = 0;

        foreach ($_SESSION['cart']['wine'] as $wineToDel) {
           // if ($wineToDel->id == $id) {
                
           // }

            $i++;
        }






        foreach ($_SESSION['cart']['wine'] as $wine) {
                 $this->table->saveWines($wine);
        }
        $this->deleteCartAction();
    }

}
