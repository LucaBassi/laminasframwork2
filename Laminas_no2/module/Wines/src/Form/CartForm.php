<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Wines\Form;

use Laminas\Form\Form;

class CartForm extends Form {

    public function __construct($name) {
        // We will ignore the name provided to the constructor
        parent::__construct('cart');
        
        foreach ($name as $input){
        
        
        
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'quantity',
            'type' => 'number',
            'options' => [
                'label' => 'Quantity',
            ],
        ]);
        }    
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id' => 'submitbutton',
            ],
        ]);
    }

}
