<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Wines\Form;

use Laminas\Form\Form;

class WinesForm extends Form {

    public function __construct($name = null) {
        // We will ignore the name provided to the constructor
        parent::__construct('wines');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'code',
            'type' => 'text',
            'options' => [
                'label' => 'Code',
            ],
        ]);
        $this->add([
            'name' => 'brand',
            'type' => 'text',
            'options' => [
                'label' => 'Brand',
            ],
        ]);
        $this->add([
            'name' => 'model',
            'type' => 'text',
            'options' => [
                'label' => 'Model',
            ],
        ]);
        $this->add([
            'name' => 'photo',
            'type' => 'text',
            'options' => [
                'label' => 'Photo',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id' => 'submitbutton',
            ],
        ]);
        $this->add([
            'name' => 'submit2',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id' => 'submitbutton',
            ],
        ]);
    }

}
