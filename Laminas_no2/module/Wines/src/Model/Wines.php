<?php
namespace Wines\Model;

use DomainException;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\InputFilter\InputFilter;
use Laminas\InputFilter\InputFilterAwareInterface;
use Laminas\InputFilter\InputFilterInterface;
use Laminas\Validator\StringLength;


class Wines implements InputFilterAwareInterface
{
    public $id;
    public $code;
    public $brand;
    public $model;
    public $price;
    public $quantity;
    public $qtyAvailable;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->code     = !empty($data['code']) ? $data['code'] : null;
        $this->brand = !empty($data['brand']) ? $data['brand'] : null;
        $this->model  = !empty($data['model']) ? $data['model'] : null;
        $this->photo  = !empty($data['photo']) ? $data['photo'] : null;
        $this->price  = !empty($data['price']) ? $data['price'] : null;
        $this->description  = !empty($data['description']) ? $data['description'] : null;
        $this->qtyAvailable  = !empty($data['qtyAvailable']) ? $data['qtyAvailable'] : null;
    }

    
      public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    public function getInputFilter()
    {
        //if ($this->inputFilter) {
          //  return $this->inputFilter;
        //}

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $inputFilter->add([
            'name' => 'code',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'brand',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);


        
        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }
    
        public function getArrayCopy()
    {
        return [
            'id'     => $this->id,
            'code' => $this->code,
            'brand'  => $this->brand,
        ];
    }

    } 